-- sys.device definition

CREATE TABLE `device` (
                          `id` int unsigned NOT NULL AUTO_INCREMENT,
                          `service_name` varchar(40) NOT NULL,
                          `location` varchar(40) NOT NULL,
                          `device_type` varchar(100) NOT NULL,
                          `ip` varchar(40) NOT NULL,
                          `admin` varchar(40) NOT NULL,
                          `password` varchar(40) NOT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;

-- sys.peopleflow definition

CREATE TABLE `peopleflow` (
                              `id` smallint NOT NULL AUTO_INCREMENT,
                              `device_id` int NOT NULL,
                              `device_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
                              `send_time` varchar(45) NOT NULL,
                              `enter_num` int NOT NULL,
                              `exit_num` int NOT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- sys.vehicle definition

CREATE TABLE `vehicle` (
                           `id` smallint NOT NULL AUTO_INCREMENT,
                           `device_id` int NOT NULL,
                           `device_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
                           `send_time` varchar(45) NOT NULL,
                           `light_vehicle` int NOT NULL,
                           `mid_vehicle` int NOT NULL,
                           `heavy_vehicle` int NOT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;