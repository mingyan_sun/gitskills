package com.example.servicearea.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.servicearea.domain.PeopleDate;
import org.apache.ibatis.annotations.Mapper;


public interface IPeople extends IService<PeopleDate> {

}
