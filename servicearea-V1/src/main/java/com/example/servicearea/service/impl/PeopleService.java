package com.example.servicearea.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.servicearea.dao.PeopleDao;
import com.example.servicearea.domain.PeopleDate;
import com.example.servicearea.service.IPeople;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PeopleService extends ServiceImpl<PeopleDao, PeopleDate> implements IPeople {

    @Autowired
    private PeopleDao peopleDao;

    public List<PeopleDate> SelectTodayPeople(){
        return peopleDao.selectPersonToday();
    }

    public List<PeopleDate> SelectPersonByTime(String time){
        return peopleDao.selectPersonByTime(time);
    }

    public List<PeopleDate> SelectPersonById(Integer id){return peopleDao.selectPersonById(id);}
}