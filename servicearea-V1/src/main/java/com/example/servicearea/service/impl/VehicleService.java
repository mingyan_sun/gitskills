package com.example.servicearea.service.impl;

import com.example.servicearea.dao.VehicleDao;
import com.example.servicearea.domain.VehicleDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleService {

    @Autowired
    private VehicleDao vehicleDao;

    public List<VehicleDate> SelectTodayVehicle(){
        return vehicleDao.selectVehicleToday();
    }

    public List<VehicleDate> SelectVehicleByTime(String Time){
        return vehicleDao.selectVehicleByTime(Time);
    }

    public List<VehicleDate> SelectVehicleById(Integer id) {return vehicleDao.selectVehicleById(id);}
}
