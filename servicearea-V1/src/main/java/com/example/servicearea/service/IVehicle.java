package com.example.servicearea.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.servicearea.domain.VehicleDate;

public interface IVehicle extends IService<VehicleDate> {
}
