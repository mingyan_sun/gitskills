package com.example.servicearea.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.servicearea.utils.Sdk;
import com.example.servicearea.dao.DeviceDao;
import com.example.servicearea.domain.DeviceDate;
import com.example.servicearea.service.IDevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeviceService extends ServiceImpl<DeviceDao, DeviceDate> implements IDevice {

    @Autowired
    private DeviceDao deviceDao;

    public List<DeviceDate> SelectDeviceInfo(Integer id){

        return deviceDao.selectDeviceInfo(id);
    }

    public void loginDevice(Integer deviceId,String ip, String admin, String password){
        Sdk.DeviceStart(deviceId,ip, admin, password);
    }

    public String selectServiceName(){
        return deviceDao.selectDeviceName();
    }

    public Integer selectDeviceState(Integer id){return deviceDao.selectDeviceState(id);}

    public Integer updateDeviceStateById(int deviceId){
        return deviceDao.updateDeviceState(deviceId);
    }
}
