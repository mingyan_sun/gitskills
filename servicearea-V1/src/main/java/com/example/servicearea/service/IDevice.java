package com.example.servicearea.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.servicearea.domain.DeviceDate;
import org.apache.ibatis.annotations.Select;

public interface IDevice extends IService<DeviceDate> {
}
