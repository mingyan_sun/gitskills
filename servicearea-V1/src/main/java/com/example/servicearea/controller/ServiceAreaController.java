package com.example.servicearea.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.servicearea.domain.DeviceDate;
import com.example.servicearea.domain.PeopleDate;
import com.example.servicearea.domain.VehicleDate;
import com.example.servicearea.service.impl.DeviceService;
import com.example.servicearea.service.impl.PeopleService;
import com.example.servicearea.service.impl.VehicleService;
import com.example.servicearea.utils.ApplicationContextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/services")
public class ServiceAreaController {
    @Value("${platform.Ip}")
    private String Ip;
    @Value("${platform.Port}")
    private String Port;


    @Autowired
    private RestTemplate restTemplate;



//    @PostConstruct
//    public void tempDeviceLogin(){
//        String [] ip = {"100.64.5.102","100.64.5.103","100.64.5.106","100.64.5.108","100.64.5.111","100.64.5.115"
//                ,"100.64.5.116","100.64.5.120","100.64.5.122","100.64.5.124","192.168.32.100","192.168.32.101"};
//        for (int i = 1; i < 13; i++) {
//            deviceService.loginDevice(i,ip[i-1],"admin","hik123456");
//            deviceService.updateDeviceStateById(i);
//        }
//    }

    @PostConstruct
    public void initDeviceLogin(){
        DeviceService deviceService = ApplicationContextUtils.getBean(DeviceService.class);
        List<DeviceDate> deviceDates = deviceService.SelectDeviceInfo(1);

        for (int i = 0; i < deviceDates.size(); i++) {
            Integer deviceState = deviceService.selectDeviceState(i+1);
            if (deviceState == 1){
                String Ip = deviceDates.get(i).getIp();
                String Admin = deviceDates.get(i).getAdmin();
                String Password = deviceDates.get(i).getPassword();
                deviceService.loginDevice(i+1, Ip, Admin, Password);
            }
            else
                continue;
        }

    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void DeviceStart(){
        DeviceDate deviceDate = ApplicationContextUtils.getBean(DeviceDate.class);
        DeviceService deviceService = ApplicationContextUtils.getBean(DeviceService.class);

        Map<String,Integer> map = new HashMap<>();
        Integer serviceId = deviceDate.getServiceId();
        System.out.println(serviceId);
        map.put("serviceId", serviceId);
        JSONArray result = restTemplate.postForObject("http://" + Ip + ":" + Port + "/Device/deviceBinding",map,JSONArray.class);
        System.out.println(result);
        for (int i=0;i <result.size();i++){
            HashMap<String,Object> jsonObject = (HashMap) result.get(i);
            Integer deviceId = (Integer) jsonObject.get("serviceLogId");
            Integer deviceState = deviceService.selectDeviceState(deviceId);
            if (deviceState == 1){
                continue;
            }
            else {
                String ip = (String) jsonObject.get("ip");
                String admin = (String) jsonObject.get("admin");
                String password = (String) jsonObject.get("password");
                deviceService.loginDevice(deviceId, ip, admin, password);
                deviceService.updateDeviceStateById(deviceId);
            }
        }
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void SelectDeviceInfo(){
        DeviceDate deviceDate = ApplicationContextUtils.getBean(DeviceDate.class);
        DeviceService deviceService = ApplicationContextUtils.getBean(DeviceService.class);

        Integer serviceId = deviceDate.getServiceId();
        String tableName = "device";
        Map<String,Object> Name = new HashMap<>();
        Name.put("tableName",tableName);
        Name.put("serviceId",serviceId);
        Integer maxId = restTemplate.postForObject("http://" + Ip + ":" + Port + "/platform/getMaxServiceLogId",Name,int.class);
        List<DeviceDate> serviceData = deviceService.SelectDeviceInfo(maxId);
        System.out.println(serviceData);
        JSONArray result = JSONArray.parseArray(JSON.toJSONString(serviceData));
        Map<String,Object> map = new HashMap<>();
        map.put("tableName",tableName);
        map.put("serviceData",result);
        map.put("serviceId", serviceId);
        restTemplate.postForObject("http://" + Ip + ":" + Port + "/platform/sendServiceAreaData",map,Integer.class);
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void SelectVehicleInfo(){
        DeviceDate deviceDate = ApplicationContextUtils.getBean(DeviceDate.class);
        VehicleService vehicleService = ApplicationContextUtils.getBean(VehicleService.class);
        String tableName = "vehicle";
        Integer serviceId = deviceDate.getServiceId();
        Map<String,Object> Name = new HashMap<>();
        Name.put("tableName",tableName);
        Name.put("serviceId",serviceId);
        Integer maxId = restTemplate.postForObject("http://" + Ip + ":" + Port + "/platform/getMaxServiceLogId",Name,int.class);
        List<VehicleDate> serviceData = vehicleService.SelectVehicleById(maxId);
        JSONArray result = JSONArray.parseArray(JSON.toJSONString(serviceData));
        Map<String,Object> map = new HashMap<>();
        map.put("tableName",tableName);
        map.put("serviceData",result);
        map.put("serviceId", serviceId);
        restTemplate.postForObject("http://" + Ip + ":" + Port + "/platform/sendServiceAreaData",map,Integer.class);
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void SelectPeopleInfo(){
        DeviceDate deviceDate = ApplicationContextUtils.getBean(DeviceDate.class);
        PeopleService peopleService = ApplicationContextUtils.getBean(PeopleService.class);
        String tableName = "peopleflow";
        Integer serviceId = deviceDate.getServiceId();
        Map<String,Object> Name = new HashMap<>();

        Name.put("tableName",tableName);
        Name.put("serviceId",serviceId);
        Integer maxId = restTemplate.postForObject("http://" + Ip + ":" + Port + "/platform/getMaxServiceLogId",Name,int.class);
        List<PeopleDate> serviceData = peopleService.SelectPersonById(maxId);

        JSONArray result = JSONArray.parseArray(JSON.toJSONString(serviceData));
        Map<String,Object> map = new HashMap<>();
        map.put("tableName",tableName);
        map.put("serviceData",result);
        map.put("serviceId", serviceId);
        restTemplate.postForObject("http://" + Ip + ":" + Port + "/platform/sendServiceAreaData",map, Integer.class);
    }

}