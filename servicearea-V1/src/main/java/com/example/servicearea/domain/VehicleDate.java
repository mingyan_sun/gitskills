package com.example.servicearea.domain;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class VehicleDate {
    private Integer id;
    private String deviceIp;
    private String sendTime;
    private String license;
    private Integer vehicleType;
    private Integer deviceId;
    @Value("${service.Id}")
    private Integer serviceId;
}