package com.example.servicearea.domain;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class PeopleDate {
    private Integer id;
    private String deviceIp;
    private String sendTime;
    private Integer enterNum;
    private Integer exitNum;
    private Integer deviceId;
    @Value("${service.Id}")
    private Integer serviceId;
}

