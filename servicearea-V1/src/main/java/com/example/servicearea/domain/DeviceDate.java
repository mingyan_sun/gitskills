package com.example.servicearea.domain;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
public class DeviceDate {
    private Integer deviceId;
    private String location;
    private String deviceType;
    private String ip;
    private String admin;
    private String password;
    private String orientation;
    private String deviceState;
    @Value("${serviceId.Id}")
    private Integer serviceId;
}
