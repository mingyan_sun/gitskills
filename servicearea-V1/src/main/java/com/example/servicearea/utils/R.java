package com.example.servicearea.utils;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class R {

    private Object Data;
    private Boolean flag;
    private String msg;

    /*public R(Object data, Boolean flag, String msg) {
        Data = data;
        this.flag = flag;
        this.msg = msg;
    }

    public R(Object data, Boolean flag) {
        Data = data;
        this.flag = flag;
    }*/
}
