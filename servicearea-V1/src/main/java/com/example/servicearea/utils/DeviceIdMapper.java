package com.example.servicearea.utils;

import java.util.HashMap;
import java.util.Map;

public class DeviceIdMapper {
    private static Map<String, Integer> deviceMap = new HashMap<>();
    public static void setDeviceMap(String ip, Integer id)
    {
        if (deviceMap.containsKey(ip)){
            deviceMap.remove(ip);
            deviceMap.put(ip, id);
        }else {
            deviceMap.put(ip,id);
        }
    }

    public static Integer getDeviceId(String ip)
    {
        return deviceMap.get(ip);
    }
}
