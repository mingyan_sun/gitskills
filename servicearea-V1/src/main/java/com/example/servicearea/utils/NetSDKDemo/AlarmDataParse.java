package com.example.servicearea.utils.NetSDKDemo;

import com.example.servicearea.utils.CommonUtil;
import com.example.servicearea.utils.DeviceIdMapper;
import com.example.servicearea.utils.RemoveZero;
import com.sun.jna.Pointer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jiangxin
 * @create 2022-08-15-18:04
 */
public class AlarmDataParse {

    private static String url = "jdbc:mysql://localhost:3306/service?useSSL=false&serverTimezone=UTC&characterEncoding=utf-8"; // 获取连接
    private static String user = "root";
    private static String password= "Root@123456!";

    public static void alarmDataHandle(int lCommand, HCNetSDK.NET_DVR_ALARMER pAlarmer, Pointer pAlarmInfo, int dwBufLen, Pointer pUser) {
        System.out.println("报警事件类型： lCommand:" + Integer.toHexString(lCommand));
        String sTime;
        String MonitoringSiteID;
        //lCommand是传的报警类型
        switch (lCommand) {
            case HCNetSDK.COMM_ITS_PLATE_RESULT://交通抓拍结果(新报警信息)
                HCNetSDK.NET_ITS_PLATE_RESULT strItsPlateResult = new HCNetSDK.NET_ITS_PLATE_RESULT();
                strItsPlateResult.write();
                Pointer pItsPlateInfo = strItsPlateResult.getPointer();
                pItsPlateInfo.write(0, pAlarmInfo.getByteArray(0, strItsPlateResult.size()), 0, strItsPlateResult.size());
                strItsPlateResult.read();
                try {
                    String License = RemoveZero.byteToStr(strItsPlateResult.struPlateInfo.sLicense);
                    String sLicense = new String(strItsPlateResult.struPlateInfo.sLicense, "GBK");
                    byte VehicleType = strItsPlateResult.byVehicleType;  //0-其他车辆，1-小型车，2-大型车，3- 行人触发，4- 二轮车触发，5- 三轮车触发，6- 机动车触发
                    MonitoringSiteID = new String(strItsPlateResult.byMonitoringSiteID);
                    String strIpV = RemoveZero.byteToStr(pAlarmer.sDeviceIP);
                    String struTime = "" + String.format("%04d", strItsPlateResult.struSnapFirstPicTime.wYear) +
                            String.format("%02d", strItsPlateResult.struSnapFirstPicTime.byMonth) +
                            String.format("%02d", strItsPlateResult.struSnapFirstPicTime.byDay) +
                            String.format("%02d", strItsPlateResult.struSnapFirstPicTime.byHour) +
                            String.format("%02d", strItsPlateResult.struSnapFirstPicTime.byMinute) +
                            String.format("%02d", strItsPlateResult.struSnapFirstPicTime.bySecond);
                    System.out.println("车牌号：" + License + ":车辆类型：" + VehicleType + ":监控点编号：" + MonitoringSiteID + "采集时间：" + struTime);
                    Integer deviceId = DeviceIdMapper.getDeviceId(strIpV);
                    try{
                        Class.forName("com.mysql.cj.jdbc.Driver");			 // 加载驱动类
                        Connection conn = DriverManager.getConnection(url, user, password);
                        System.out.println("callback" + deviceId);
                        String sql = "insert into vehicle(device_ip,send_time,license,device_id,vehicle_type) " +
                                "values( '"+ strIpV+"'," +
                                "'"+struTime+"'," +
                                "'"+License+"'," +
                                "'"+deviceId+"'," +
                                "'"+VehicleType+"'" +
                                ")";  //******
                        // 根据页面的数据，生成插入学生的sql语句
                        Statement stmt = conn.createStatement();// 创建statement
                        int count = stmt.executeUpdate(sql);// 执行sql
                        String message = "插入失败";
                        if(count>0){
                            message = "插入成功";
                        }
                        //JOptionPane.showMessageDialog(null, message);  //消息对话框
                        conn.close();
                        stmt.close();
                    }catch(Exception e2){
                        e2.printStackTrace();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                /**
                 * 报警图片保存，车牌，车辆图片
                 */
                /*for (int i = 0; i < strItsPlateResult.dwPicNum; i++) {
                    if (strItsPlateResult.struPicInfo[i].dwDataLen > 0) {
                        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                        String newName = sf.format(new Date());
                        FileOutputStream fout;
                        try {
                            String filename = "../pic/" + newName + "_type[" + strItsPlateResult.struPicInfo[i].byType + "]_ItsPlate.jpg";
                            fout = new FileOutputStream(filename);
                            //将字节写入文件
                            long offset = 0;
                            ByteBuffer buffers = strItsPlateResult.struPicInfo[i].pBuffer.getByteBuffer(offset, strItsPlateResult.struPicInfo[i].dwDataLen);
                            byte[] bytes = new byte[strItsPlateResult.struPicInfo[i].dwDataLen];
                            buffers.rewind();
                            buffers.get(bytes);
                            fout.write(bytes);
                            fout.close();
                        } catch (FileNotFoundException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }*/
                break;
            case HCNetSDK.COMM_ALARM_TPS_V41://交通数据统计的报警
                HCNetSDK.NET_DVR_TPS_ALARM_V41 strTPSalarmV41 = new HCNetSDK.NET_DVR_TPS_ALARM_V41();
                strTPSalarmV41.write();
                Pointer pstrTPSalarmV41 = strTPSalarmV41.getPointer();
                pstrTPSalarmV41.write(0, pAlarmInfo.getByteArray(0, strTPSalarmV41.size()), 0, strTPSalarmV41.size());
                strTPSalarmV41.read();
                sTime = CommonUtil.parseTime(strTPSalarmV41.dwAbsTime);
                MonitoringSiteID = strTPSalarmV41.byMonitoringSiteID.toString(); //监控点编号
                String StartTime = CommonUtil.parseTime(strTPSalarmV41.dwStartTime); //开始统计时间；
                String StopTime = CommonUtil.parseTime(strTPSalarmV41.dwStopTime); //结束统计时间；
                System.out.println("【交通数据统计】" + "时间：" + sTime + "监控点编号：" + MonitoringSiteID + "开始统计时间：" + StartTime + "结束统计时间：" + StopTime);
                //车道统计参数信息
                for (int i = 0; i <= HCNetSDK.MAX_TPS_RULE; i++) {
                    byte LaneNo = strTPSalarmV41.struTPSInfo.struLaneParam[i].byLaneNo; //车道号
                    byte TrafficState = strTPSalarmV41.struTPSInfo.struLaneParam[i].byTrafficState; //车道状态 0-无效，1-畅通，2-拥挤，3-堵塞
                    int TpsType = strTPSalarmV41.struTPSInfo.struLaneParam[i].dwTpsType; //数据变化类型标志，表示当前上传的统计参数中，哪些数据有效，按位区分
                    int LaneVolume = strTPSalarmV41.struTPSInfo.struLaneParam[i].dwLaneVolume; //车道流量
                    int LaneVelocity = strTPSalarmV41.struTPSInfo.struLaneParam[i].dwLaneVelocity; //车道平均速度
                    float SpaceOccupyRation = strTPSalarmV41.struTPSInfo.struLaneParam[i].fSpaceOccupyRation;  //车道占有率，百分比计算（空间上，车辆长度与监控路段总长度的比值)
                    System.out.println("车道号：" + LaneNo + "车道状态：" + TrafficState + "车道流量：" + LaneVolume + "车道占有率：" + SpaceOccupyRation + "\n");
                }
                break;
            case HCNetSDK.COMM_ALARM_TPS_REAL_TIME: //实时过车数据数据
                HCNetSDK.NET_DVR_TPS_REAL_TIME_INFO netDvrTpsParam = new HCNetSDK.NET_DVR_TPS_REAL_TIME_INFO();
                netDvrTpsParam.write();
                Pointer pItsParkVehicle = netDvrTpsParam.getPointer();
                pItsParkVehicle.write(0, pAlarmInfo.getByteArray(0, netDvrTpsParam.size()), 0, netDvrTpsParam.size());
                netDvrTpsParam.read();
                String struTime = "" + String.format("%04d", netDvrTpsParam.struTime.wYear) +
                        String.format("%02d", netDvrTpsParam.struTime.byMonth) +
                        String.format("%02d", netDvrTpsParam.struTime.byDay) +
                        String.format("%02d", netDvrTpsParam.struTime.byHour) +
                        String.format("%02d", netDvrTpsParam.struTime.byMinute) +
                        String.format("%02d", netDvrTpsParam.struTime.bySecond);
                Short wDeviceID = netDvrTpsParam.struTPSRealTimeInfo.wDeviceID;//设备ID
                int channel = netDvrTpsParam.dwChan; //触发报警通道号
                String byLane = new String(String.valueOf(netDvrTpsParam.struTPSRealTimeInfo.byLane)).trim();// 对应车道号
                String bySpeed = new String(String.valueOf(netDvrTpsParam.struTPSRealTimeInfo.bySpeed)).trim();// 对应车速（KM/H)
                int dwDownwardFlow = netDvrTpsParam.struTPSRealTimeInfo.dwDownwardFlow;//当前车道 从上到下车流量
                int dwUpwardFlow = netDvrTpsParam.struTPSRealTimeInfo.dwUpwardFlow;       //当前车道 从下到上车流量
                System.out.println("通道号：" + channel + "; 时间：" + struTime + ";对应车道号：" + byLane + ";当前车道 从上到下车流量：" + dwDownwardFlow + ";dwUpwardFlow：" + dwUpwardFlow);
                break;

            case HCNetSDK.COMM_ALARM_TPS_STATISTICS: //统计过车数据
                HCNetSDK.NET_DVR_TPS_STATISTICS_INFO netDvrTpsStatisticsInfo = new HCNetSDK.NET_DVR_TPS_STATISTICS_INFO();
                netDvrTpsStatisticsInfo.write();
                Pointer pTpsVehicle = netDvrTpsStatisticsInfo.getPointer();
                pTpsVehicle.write(0, pAlarmInfo.getByteArray(0, netDvrTpsStatisticsInfo.size()), 0, netDvrTpsStatisticsInfo.size());
                netDvrTpsStatisticsInfo.read();
                int Tpschannel = netDvrTpsStatisticsInfo.dwChan; //触发报警通道号
                //统计开始时间
                String struStartTime = "" + String.format("%04d", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struStartTime.wYear) +
                        String.format("%02d", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struStartTime.byMonth) +
                        String.format("%02d", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struStartTime.byDay) +
                        String.format("%02d", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struStartTime.byHour) +
                        String.format("%02d", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struStartTime.byMinute) +
                        String.format("%02d", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struStartTime.bySecond);
                byte TotalLaneNum = netDvrTpsStatisticsInfo.struTPSStatisticsInfo.byTotalLaneNum; //有效车道总数

                String struVehicle = String.format("小型车: %02d,", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struLaneParam.dwLightVehicle) +
                        String.format("中型车: %02d,", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struLaneParam.dwMidVehicle) +
                        String.format("大型车: %02d", netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struLaneParam.dwHeavyVehicle);
                //System.out.println(struVehicle);
                String VehicleIp = new String(pAlarmer.sDeviceIP,StandardCharsets.UTF_8);
                String strIpV = RemoveZero.byteToStr(pAlarmer.sDeviceIP);
                try{
                    Class.forName("com.mysql.cj.jdbc.Driver");			 // 加载驱动类
                    Connection conn = DriverManager.getConnection(url, user, password);
                    String sql = "insert into vehicle(device_id,device_ip,send_time,light_vehicle,mid_vehicle,heavy_vehicle) " +
                            "values('"+netDvrTpsStatisticsInfo.struTPSStatisticsInfo.wDeviceID+"'," +
                            "'"+ strIpV+"'," +
                            "'"+struStartTime+"'," +
                            "'"+netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struLaneParam.dwLightVehicle+"'," +
                            "'"+netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struLaneParam.dwMidVehicle+"'," +
                            "'"+netDvrTpsStatisticsInfo.struTPSStatisticsInfo.struLaneParam.dwHeavyVehicle+"')";  //******
                    // 根据页面的数据，生成插入学生的sql语句
                    Statement stmt = conn.createStatement();// 创建statement
                    int count = stmt.executeUpdate(sql);// 执行sql
                    String message = "插入失败";
                    if(count>0){
                        message = "插入成功";
                    }
                    //JOptionPane.showMessageDialog(null, message);  //消息对话框
                    conn.close();
                    stmt.close();
                }catch(Exception e2){
                    e2.printStackTrace();
                }

                System.out.println("通道号：" + Tpschannel + "; 开始统计时间：" + struStartTime + "; 有效车道总数：" + TotalLaneNum + "; 经过车辆：" + struVehicle);
                break;
            case HCNetSDK.COMM_ITS_PARK_VEHICLE: //停车场数据上传
                HCNetSDK.NET_ITS_PARK_VEHICLE strParkVehicle = new HCNetSDK.NET_ITS_PARK_VEHICLE();
                strParkVehicle.write();
                Pointer pstrParkVehicle = strParkVehicle.getPointer();
                pstrParkVehicle.write(0, pAlarmInfo.getByteArray(0, strParkVehicle.size()), 0, strParkVehicle.size());
                strParkVehicle.read();
                try {
                    byte ParkError = strParkVehicle.byParkError; //停车异常：0- 正常，1- 异常
                    String ParkingNo = new String(strParkVehicle.byParkingNo, "UTF-8"); //车位编号
                    byte LocationStatus = strParkVehicle.byLocationStatus; //车位车辆状态 0- 无车，1- 有车
                    MonitoringSiteID = strParkVehicle.byMonitoringSiteID.toString();
                    String plateNo = new String(strParkVehicle.struPlateInfo.sLicense, "GBK"); //车牌号
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                //报警图片信息
                for (int i = 0; i < strParkVehicle.dwPicNum; i++) {
                    if (strParkVehicle.struPicInfo[i].dwDataLen > 0) {
                        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                        String newName = sf.format(new Date());
                        FileOutputStream fout;
                        try {
                            String filename = "../pic/" + newName + "_ParkVehicle.jpg";
                            fout = new FileOutputStream(filename);
                            //将字节写入文件
                            long offset = 0;
                            ByteBuffer buffers = strParkVehicle.struPicInfo[i].pBuffer.getByteBuffer(offset, strParkVehicle.struPicInfo[i].dwDataLen);
                            byte[] bytes = new byte[strParkVehicle.struPicInfo[i].dwDataLen];
                            buffers.rewind();
                            buffers.get(bytes);
                            fout.write(bytes);
                            fout.close();
                        } catch (FileNotFoundException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
                break;



            //行为分析信息
            case HCNetSDK.COMM_ALARM_RULE:
                HCNetSDK.NET_VCA_RULE_ALARM strVcaAlarm = new HCNetSDK.NET_VCA_RULE_ALARM();
                strVcaAlarm.write();
                Pointer pVCAInfo = strVcaAlarm.getPointer();
                pVCAInfo.write(0, pAlarmInfo.getByteArray(0, strVcaAlarm.size()), 0, strVcaAlarm.size());
                strVcaAlarm.read();

                switch (strVcaAlarm.struRuleInfo.wEventTypeEx) {
                    case 1: //穿越警戒面 (越界侦测)
                        System.out.println("越界侦测报警发生");
                        strVcaAlarm.struRuleInfo.uEventParam.setType(HCNetSDK.NET_VCA_TRAVERSE_PLANE.class);
                        System.out.println("检测目标："+strVcaAlarm.struRuleInfo.uEventParam.struTraversePlane.byDetectionTarget); //检测目标，0表示所有目标（表示不锁定检测目标，所有目标都将进行检测），其他取值按位表示不同的检测目标：0x01-人，0x02-车
                        //图片保存
                        if ((strVcaAlarm.dwPicDataLen > 0) && (strVcaAlarm.byPicTransType == 0)) {
                            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                            String newName = sf.format(new Date());
                            FileOutputStream fout;
                            try {
                                String filename = "../pic/" + newName + "VCA_TRAVERSE_PLANE" + ".jpg";
                                fout = new FileOutputStream(filename);
                                //将字节写入文件
                                long offset = 0;
                                ByteBuffer buffers = strVcaAlarm.pImage.getByteBuffer(offset, strVcaAlarm.dwPicDataLen);
                                byte[] bytes = new byte[strVcaAlarm.dwPicDataLen];
                                buffers.rewind();
                                buffers.get(bytes);
                                fout.write(bytes);
                                fout.close();
                            } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 2: //目标进入区域
                        System.out.println("目标进入区域报警发生");
                        strVcaAlarm.struRuleInfo.uEventParam.setType(HCNetSDK.NET_VCA_AREA.class);
                        System.out.println("检测目标："+strVcaAlarm.struRuleInfo.uEventParam.struArea.byDetectionTarget);
                        //图片保存
                        if ((strVcaAlarm.dwPicDataLen > 0) && (strVcaAlarm.byPicTransType == 0)) {
                            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                            String newName = sf.format(new Date());
                            FileOutputStream fout;
                            try {
                                String filename = "../pic/" + newName + "_TargetEnter" + ".jpg";
                                fout = new FileOutputStream(filename);
                                //将字节写入文件
                                long offset = 0;
                                ByteBuffer buffers = strVcaAlarm.pImage.getByteBuffer(offset, strVcaAlarm.dwPicDataLen);
                                byte[] bytes = new byte[strVcaAlarm.dwPicDataLen];
                                buffers.rewind();
                                buffers.get(bytes);
                                fout.write(bytes);
                                fout.close();
                            } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 3: //目标离开区域
                        System.out.println("目标离开区域报警触发");
                        strVcaAlarm.struRuleInfo.uEventParam.setType(HCNetSDK.NET_VCA_AREA.class);
                        System.out.println("检测目标："+strVcaAlarm.struRuleInfo.uEventParam.struArea.byDetectionTarget);
                        //图片保存
                        if ((strVcaAlarm.dwPicDataLen > 0) && (strVcaAlarm.byPicTransType == 0)) {
                            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                            String newName = sf.format(new Date());
                            FileOutputStream fout;
                            try {
                                String filename = "../pic/" + newName + "_TargetLeave" + ".jpg";
                                fout = new FileOutputStream(filename);
                                //将字节写入文件
                                long offset = 0;
                                ByteBuffer buffers = strVcaAlarm.pImage.getByteBuffer(offset, strVcaAlarm.dwPicDataLen);
                                byte[] bytes = new byte[strVcaAlarm.dwPicDataLen];
                                buffers.rewind();
                                buffers.get(bytes);
                                fout.write(bytes);
                                fout.close();
                            } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 4: //周界入侵
                        System.out.println("周界入侵报警发生");
                        strVcaAlarm.struRuleInfo.uEventParam.setType(HCNetSDK.NET_VCA_INTRUSION.class);
                        System.out.println("检测目标："+strVcaAlarm.struRuleInfo.uEventParam.struIntrusion.byDetectionTarget);
                        //图片保存
                        if ((strVcaAlarm.dwPicDataLen > 0) && (strVcaAlarm.byPicTransType == 0)) {
                            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                            String newName = sf.format(new Date());
                            FileOutputStream fout;
                            try {
                                String filename = "../pic/" + newName + "VCA_INTRUSION" + ".jpg";
                                fout = new FileOutputStream(filename);
                                //将字节写入文件
                                long offset = 0;
                                ByteBuffer buffers = strVcaAlarm.pImage.getByteBuffer(offset, strVcaAlarm.dwPicDataLen);
                                byte[] bytes = new byte[strVcaAlarm.dwPicDataLen];
                                buffers.rewind();
                                buffers.get(bytes);
                                fout.write(bytes);
                                fout.close();
                            } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 5: //徘徊
                        System.out.println("徘徊事件触发");

                        break;
                    case 8: //快速移动(奔跑)，
                        System.out.println("快速移动(奔跑)事件触发");
                        break;
                    case 15: //离岗
                        System.out.println("离岗事件触发");
                        strVcaAlarm.struRuleInfo.uEventParam.setType(HCNetSDK.NET_VCA_LEAVE_POSITION.class);
                        System.out.println(strVcaAlarm.struRuleInfo.uEventParam.struLeavePos.byOnPosition);
                        //图片保存
                        if ((strVcaAlarm.dwPicDataLen > 0) && (strVcaAlarm.byPicTransType == 0)) {
                            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                            String newName = sf.format(new Date());
                            FileOutputStream fout;
                            try {
                                String filename = "../pic/" + newName + "VCA_LEAVE_POSITION_" + ".jpg";
                                fout = new FileOutputStream(filename);
                                //将字节写入文件
                                long offset = 0;
                                ByteBuffer buffers = strVcaAlarm.pImage.getByteBuffer(offset, strVcaAlarm.dwPicDataLen);
                                byte[] bytes = new byte[strVcaAlarm.dwPicDataLen];
                                buffers.rewind();
                                buffers.get(bytes);
                                fout.write(bytes);
                                fout.close();
                            } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    case 20: //倒地检测
                        System.out.println("倒地事件触发");
                        break;
                    case 44: //玩手机

                        System.out.println("玩手机报警发生");
                        //图片保存
                        if ((strVcaAlarm.dwPicDataLen > 0) && (strVcaAlarm.byPicTransType == 0)) {
                            SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
                            String newName = sf.format(new Date());
                            FileOutputStream fout;
                            try {
                                String filename = "../pic/" + newName + "PLAY_CELLPHONE_" + ".jpg";
                                fout = new FileOutputStream(filename);
                                //将字节写入文件
                                long offset = 0;
                                ByteBuffer buffers = strVcaAlarm.pImage.getByteBuffer(offset, strVcaAlarm.dwPicDataLen);
                                byte[] bytes = new byte[strVcaAlarm.dwPicDataLen];
                                buffers.rewind();
                                buffers.get(bytes);
                                fout.write(bytes);
                                fout.close();
                            } catch (FileNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                        break;
                    case 45: //持续检测
                        System.out.println("持续检测事件触发");
                    default:
                        System.out.println("行为事件类型:" + strVcaAlarm.struRuleInfo.wEventTypeEx);
                        break;
                }
                break;

            //  客流量报警信息
            case HCNetSDK.COMM_ALARM_PDC:
                HCNetSDK.NET_DVR_PDC_ALRAM_INFO strPDCResult = new HCNetSDK.NET_DVR_PDC_ALRAM_INFO();
                strPDCResult.write();
                Pointer pPDCInfo = strPDCResult.getPointer();
                pPDCInfo.write(0, pAlarmInfo.getByteArray(0, strPDCResult.size()), 0, strPDCResult.size());
                strPDCResult.read();
                // byMode=0-实时统计结果(联合体中struStatFrame有效)，
                if (strPDCResult.byMode == 0) {
                    strPDCResult.uStatModeParam.setType(HCNetSDK.NET_DVR_STATFRAME.class);
                    String sAlarmPDC0Info = "实时客流量统计，进入人数：" + strPDCResult.dwEnterNum + "，离开人数：" + strPDCResult.dwLeaveNum +
                            ", byMode:" + strPDCResult.byMode + ", dwRelativeTime:" + strPDCResult.uStatModeParam.struStatFrame.dwRelativeTime +
                            ", dwAbsTime:" + strPDCResult.uStatModeParam.struStatFrame.dwAbsTime;
                    System.out.println(strPDCResult.dwEnterNum);
                    System.out.println(strPDCResult.dwLeaveNum);
                    System.out.println(strPDCResult.byMode);
                }
                // byMode=1-周期统计结果(联合体中struStatTime有效)，
                if (strPDCResult.byMode == 1) {
                    strPDCResult.uStatModeParam.setType(HCNetSDK.NET_DVR_STATTIME.class);
                    String strtmStart = "" + String.format("%04d", strPDCResult.uStatModeParam.struStatTime.tmStart.dwYear) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmStart.dwMonth) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmStart.dwDay) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmStart.dwHour) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmStart.dwMinute) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmStart.dwSecond);
                    String strtmEnd = "" + String.format("%04d", strPDCResult.uStatModeParam.struStatTime.tmEnd.dwYear) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmEnd.dwMonth) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmEnd.dwDay) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmEnd.dwHour) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmEnd.dwMinute) +
                            String.format("%02d", strPDCResult.uStatModeParam.struStatTime.tmEnd.dwSecond);
                    String sAlarmPDC1Info = "周期性客流量统计，进入人数：" + strPDCResult.dwEnterNum + "，离开人数：" + strPDCResult.dwLeaveNum +
                            ", byMode:" + strPDCResult.byMode + ", tmStart:" + strtmStart + ",tmEnd :" + strtmEnd;
                    System.out.println("进入人数" + strPDCResult.dwEnterNum);
                    System.out.println("离开人数" + strPDCResult.dwLeaveNum);
                    System.out.println("设定模式" + strPDCResult.byMode);
                    System.out.println(sAlarmPDC1Info);

                    String PeopleIp = new String(pAlarmer.sDeviceIP, StandardCharsets.UTF_8);
                    String strIpP = RemoveZero.byteToStr(pAlarmer.sDeviceIP);
                    System.out.println(strIpP);
                    try{
                        Class.forName("com.mysql.cj.jdbc.Driver");			 // 加载驱动类
                        Connection conn = DriverManager.getConnection(url, user, password);
                        String sql = "insert into peopleflow(device_ip,device_id,send_time,enter_num,exit_num) " +
                                "values('"+ strIpP +"','"+DeviceIdMapper.getDeviceId(strIpP)+"','"+strtmStart+"','"+strPDCResult.dwEnterNum+"','"+strPDCResult.dwLeaveNum+"')";  //******
                        // 根据页面的数据，生成插入学生的sql语句

                        Statement stmt = conn.createStatement();// 创建statement
                        int count = stmt.executeUpdate(sql);// 执行sql
                        String message = "插入失败";
                        if(count>0){
                            message = "插入成功";
                        }
                        //JOptionPane.showMessageDialog(null, message);  //消息对话框
                        conn.close();
                        stmt.close();
                    }catch(Exception e2){
                        e2.printStackTrace();
                    }


                }
                break;
            case HCNetSDK.COMM_ALARM_V30:  //移动侦测、视频丢失、遮挡、IO信号量等报警信息(V3.0以上版本支持的设备)
                HCNetSDK.NET_DVR_ALARMINFO_V30 struAlarmInfo = new HCNetSDK.NET_DVR_ALARMINFO_V30();
                struAlarmInfo.write();
                Pointer pAlarmInfo_V30 = struAlarmInfo.getPointer();
                pAlarmInfo_V30.write(0, pAlarmInfo.getByteArray(0, struAlarmInfo.size()), 0, struAlarmInfo.size());
                struAlarmInfo.read();
                System.out.println("报警类型：" + struAlarmInfo.dwAlarmType);  // 3-移动侦测
                break;
            case HCNetSDK.COMM_ALARM_V40: //移动侦测、视频丢失、遮挡、IO信号量等报警信息，报警数据为可变长
                HCNetSDK.NET_DVR_ALARMINFO_V40 struAlarmInfoV40 = new HCNetSDK.NET_DVR_ALARMINFO_V40();
                struAlarmInfoV40.write();
                Pointer pAlarmInfoV40 = struAlarmInfoV40.getPointer();
                pAlarmInfoV40.write(0, pAlarmInfo.getByteArray(0, struAlarmInfoV40.size()), 0, struAlarmInfoV40.size());
                struAlarmInfoV40.read();
                System.out.println("报警类型:" + struAlarmInfoV40.struAlarmFixedHeader.dwAlarmType); //3-移动侦测
                break;
            default:
                System.out.println("报警类型" + Integer.toHexString(lCommand));
                break;
        }
    }
}
