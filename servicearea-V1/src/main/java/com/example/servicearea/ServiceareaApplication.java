package com.example.servicearea;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@SpringBootApplication
@EnableScheduling
@MapperScan("com.example.servicearea.dao")
public class ServiceareaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceareaApplication.class, args);
    }

}
