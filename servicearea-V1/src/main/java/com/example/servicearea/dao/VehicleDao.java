package com.example.servicearea.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.servicearea.domain.VehicleDate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface VehicleDao extends BaseMapper<VehicleDate> {

    @Select("select * from vehicle where send_time BETWEEN CONCAT(CURDATE(),' 00:00:00') and now()")
    List<VehicleDate> selectVehicleToday();

    @Select("select * from vehicle where send_time BETWEEN #{time} and now()")
    List<VehicleDate> selectVehicleByTime(String time);

    @Select("select id,device_ip,send_time,license,vehicle_type ,v.device_id  from vehicle v, device d where id >= #{id} and v.device_ip = d.ip")
    List<VehicleDate> selectVehicleById(Integer id);

}

