package com.example.servicearea.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.servicearea.domain.PeopleDate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;


@Mapper
public interface PeopleDao extends BaseMapper<PeopleDate> {

    @Select("select * from peopleflow where send_time BETWEEN CONCAT(CURDATE(),' 00:00:00') and now()")
    List<PeopleDate> selectPersonToday();

    @Select("select * from peopleflow where send_time BETWEEN #{time} and now()")
    List<PeopleDate> selectPersonByTime(String time);

    @Select("Select id,device_ip ,send_time ,enter_num ,exit_num,p.device_id from peopleflow p ,device d where id >= #{id} and p.device_ip = d.ip")
    List<PeopleDate> selectPersonById(Integer id);
}