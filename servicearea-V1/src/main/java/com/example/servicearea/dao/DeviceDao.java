package com.example.servicearea.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.servicearea.domain.DeviceDate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DeviceDao extends BaseMapper<DeviceDate> {

    @Select("select * from device where device_id >= #{id}")
    List<DeviceDate> selectDeviceInfo(Integer id);

    @Select("select distinct service_name from device")
    String selectDeviceName();

    @Select("select device_state from device where device_id = #{id}")
    Integer selectDeviceState(Integer id);

    @Select("UPDATE  device SET device_state=1 WHERE device_id= #{deviceId};")
    Integer updateDeviceState(int deviceId);


}
