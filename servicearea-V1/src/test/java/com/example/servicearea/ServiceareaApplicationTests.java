package com.example.servicearea;

import com.example.servicearea.domain.DeviceDate;
import com.example.servicearea.service.IDevice;
import com.example.servicearea.service.impl.DeviceService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ServiceareaApplicationTests {

    @Autowired
    DeviceService iDevice;
    @Autowired
    DeviceDate deviceDate;
    @Test
    void contextLoads() {
    }

    @Test
    void getDeviceInfo()
    {
        List<DeviceDate> deviceDates = iDevice.SelectDeviceInfo(1);
        System.out.println(deviceDates);
    }

}
